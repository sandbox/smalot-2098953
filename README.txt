Search PDF

Specifically designed to index pdf files, Search PDF in built on PDF Parser, 
an opensource php library.
PDF Files are parsed and the text is extracted from each page.
The main advantage from this module is that it doesn't required external tool.


REQUIREMENTS
============

The main requirement is PHP 5.3 due to the use of namespace into 
PDF Parser library.
To provide greater flexibility to update the third part (PDF Parser), 
we made the choice to use the composer_manager module.


INSTALLATION
============

Please refers to the composer_manager module before installing this module.
Once done, go to the setup interface and select each content type to look 
for pdf attachments.
During the next cron run, all remaining pdf files will be indexed, but only
those linked to content type enabled will be used to build the search result.


MAINTAINER
==========

- smalot (Sebastien MALOT)

