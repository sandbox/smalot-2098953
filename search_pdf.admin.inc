<?php

/**
 * @file
 * Admin settings
 */

require_once dirname(__FILE__) . '/search_pdf.inc';

/**
 * Page callback to show the settings for the pdf.
 */
function search_pdf_settings_form() {
  $form = array();

  // File settings.
  $form['files'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Types'),
  );

  // TODO : list all content types.

  $form['files']['search_pdf_exclude_content_types'] = array(
    '#type' => 'textfield',
    '#title' => t('Exclude content types'),
    '#default_value' => variable_get('search_pdf_exclude_content_types', ''),
  );

  return system_settings_form($form);
}

/**
 * Validation handler for the settings form.
 */
function search_pdf_settings_form_validate($form, &$form_state) {
}

