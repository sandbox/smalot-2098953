<?php

/**
 * @file
 * Helper functions, to hold the .module file clean and smart.
 */

/**
 * Shutdown function to make sure we remember the last element processed.
 */
function search_pdf_shutdown() {
  global $_search_pdf_last_change, $_search_pdf_last_id;
  if ($_search_pdf_last_change && $_search_pdf_last_id) {
    variable_set('search_pdf_cron_last_change', $_search_pdf_last_change);
    variable_set('search_pdf_cron_last_id', $_search_pdf_last_id);
  }
}

/**
 * Load the files to be indexed.
 *
 * @param int $last_change
 *   The timestamp of the last indexed file.
 * @param int $last_id
 *   The fid of the last indexed file.
 *
 * @return array
 *   The array with the file objects.
 */
function search_pdf_get_files($last_change = 0, $last_id = 0) {
  $limit = (int) variable_get('search_cron_limit', 10);
  $fids = array();

  $query = db_select('file_managed', 'f');
  $query->fields('f', array('fid'))
        ->condition('f.status', 1)
        ->condition(db_or()
          ->condition('f.fid', $last_id, '>')
          ->condition('f.timestamp', $last_change), '>')
        ->orderBy('f.timestamp')
        ->range(0, $limit);
  $results = $query->execute();

  foreach ($results as $record) {
    $fids[] = $record->fid;
  }

  $files = !empty($fids) ? file_load_multiple($fids) : array();
  return $files;
}

/**
 * Extract the content of the given file.
 *
 * @param object $file
 *   The file object where the content should be extracted.
 *
 * @return string
 *   The extracted file content.
 */
function search_pdf_get_file_content($file) {

  if ($file->filemime == 'application/pdf') {
    $content = search_pdf_extract_text($file);
  }

  return (string) $content;
}

/**
 * Extract text from pdf file.
 *
 * @param object $file
 *   The file object.
 *
 * @return mixed|string
 *   The extracted content.
 */
function search_pdf_extract_text($file) {
  try {
    $filename = drupal_realpath($file->uri);
    $parser   = new \Smalot\PdfParser\Parser();
    $pdf      = $parser->parseFile($filename);
    $content  = $pdf->getText();
    $content  = htmlspecialchars(html_entity_decode($content, ENT_NOQUOTES, 'UTF-8'), ENT_NOQUOTES, 'UTF-8');
    $content  = trim($content);
    
    return $content;
  }
  catch(Exception $e) {
    return '';
  }
}

